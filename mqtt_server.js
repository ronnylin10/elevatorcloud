// before running this server,
// run sudo docker run -d -p 27017:27017 -v /home/ronny/Desktop/sdmc/js/mqtt_broker/data:/data/db mongo
// to start the database for persistence.
// Change the local directory "data" to where the database should be stored.

const config = require('./config.json')
const fs = require('fs')
const output = fs.createWriteStream('./stdout.log')
const errorOutput = fs.createWriteStream('./stderr.log')
const logger = new console.Console(output, errorOutput, false)
require('console-stamp')(console, {
  colors: {
    stamp: 'yellow',
    label: 'white'
  }
})
require('console-stamp')(logger, {
  stdout: output,
  stderr: errorOutput
})
let loggers = {}
loggers.log = function () {
  console.log(...arguments)
  logger.log(...arguments)
}
// let aedesPersistenceDB = require('aedes-persistence-mongodb')
// let persistence = aedesPersistenceDB({
//   url: config.persistence.url + config.persistence.broker_database
// })
// let mqmongo = require('mqemitter-mongodb')
// let emitter = mqmongo({
//   url: config.persistence.url + config.persistence.broker_database
// })
// let aedesOpt = {
//   mq: emitter,
//   persistence: persistence,
//   concurrency: config.aedesOpt.concurrency,
//   connectTimeout: config.aedesOpt.connectTimeout
// }
let aedes = require('aedes')()

function initializeServer (mqttConnectionUrl) {
  var splitUrl = mqttConnectionUrl.split(':')
  var protocol = splitUrl[0]
  var url = splitUrl[1].substring(2)
  var port = splitUrl[2]
  if (protocol === 'mqtts') {
    const key = fs.readFileSync(config.sslPaths.privateKey)
    const cert = fs.readFileSync(config.sslPaths.certificate)
    const ca = fs.readFileSync(config.sslPaths.caBundle)

    const serverPort = port
    var mqttServerOptions =
        {
          key: key,
          cert: cert,
          ca: ca,
          rejectUnauthorized: config.serverOpt.rejectUnauthorized,
          protocol: protocol,
          host: url
        }
    const server = require('tls').createServer(mqttServerOptions, aedes.handle)
    server.listen(serverPort, function () {
      loggers.log('server listening on port', serverPort)
    })
  } else {
    loggers.log('port number:', port)
    const serverPort = port
    const server = require('net').createServer(aedes.handle)
    server.listen(serverPort, function () {
      loggers.log('server listening on port', serverPort)
    })
  }
}

initializeServer(config.serverOpt.url)

aedes.on('client', client => {
  loggers.log(`Client [${client.id}] connected`)
})

aedes.on('clientDisconnect', client => {
  loggers.log(`Client [${client.id}] disconnected`)
})

aedes.on('clientError', (client, err) => {
  loggers.log(`Client [${client.id}] encountered error: ${JSON.stringify(err)}`)
})

aedes.on('publish', (packet, client) => {
  if (client) {
    loggers.log(`Client [${client.id}] published on ${packet.topic}: ${packet.payload}`)
  } else {
    loggers.log(`aedes published on ${packet.topic}: ${packet.payload}`)
  }
})

aedes.on('subscribe', (subscriptions, client) => {
  var subscriptionArr = subscriptions.map(subscription => {
    return `${subscription['topic']} (${subscription['qos']})`
  })
  client ? loggers.log(`Client [${client.id}] subscribed ${subscriptionArr}`)
    : loggers.log(`aedes subscribed ${subscriptions.topic}: ${subscriptions.payload}`)
})

aedes.on('unsubscribe', (unsubscriptions, client) => {
  client ? loggers.log(`Client [${client.id}] unsubscribe ${unsubscriptions.topic}`) : loggers.log('')
})
// emitter.on('event/+/+/+', (message, done) => {
//     loggers.log(">>>>>>>", message);
//     done();
// })
// var msg = {
//     topic: 'event/1/2/3',
//     payload: 'aedes on!'
// }
// emitter.emit(msg, () =>  {
//     loggers.log("emitted")
// })
