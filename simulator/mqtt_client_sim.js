'use strict'

const mqtt = require('mqtt')

// var mqttClientOptions = {
//     // rejectUnauthorized: true, //set this to false if your are using self-signed certificate
//     port: 8883,
//     protocol: 'mqtts',
//     host: "localhost"
//   };

const fs = require('fs')
const args = process.argv.slice(2)
const pubOpt = { qos: 1 }

let thisDevice = JSON.parse(fs.readFileSync(args[0], 'utf8'))
let options =
    {
      reconnectPeriod: 1000,
      resubscribe: true,
      clientId: thisDevice.serialNumber,
      clean: false,
      will: {
        topic: 'client/' + thisDevice.serialNumber + '/connect',
        payload: 'Offline',
        qos: 1,
        retain: true
      }
    }

// const client = mqtt.connect('mqtt://dev.altumview.com:1883', options)
const client = mqtt.connect('mqtts://elevate.altumview.com:1883', options)

const lockfile = require('proper-lockfile')
const path = require('path')

// Uses regex to fetch subtopics in an array
function topicSubdata (name, start) {
  name = name.replace(start, '')
  let patt = RegExp('/([a-zA-Z0-9-s]+)', 'g')
  let result = []
  let match
  while ((match = patt.exec(name)) != null) {
    result.push(match[1])
  }
  return result
}

// Writes the current device data to the this_camera_data.txt file
function writeDevice (obj) {
  console.log('writing device')
  while (true) {
    try {
      if (lockfile.checkSync(args[0])) {
        console.log('checked locking is true')
        setTimeout(function () {
          console.log('timing out')
        }, 1000)
      } else {
        console.log('checked locking is false so start writing')
        let release = lockfile.lockSync(args[0])
        fs.writeFileSync(args[0], JSON.stringify(obj))
        console.log('writing finished')
        return release()
      }
    } catch (error) {
      console.log('error in writing: ' + error)
      return
    }
  }
}

// TODO: this can optionally be a serial code

if (!thisDevice.wifiStrength) {
  thisDevice.wifiStrength = 0
}

if (!thisDevice.faces) {
  thisDevice.faces = []
}

// function getRandomId(max, min){
//     let number = Math.random();
//     while(number < 0.1){
//         number = Math.random();
//         console.log(number)
//     }
//     return (number * (max - min) + min)
// }

// console.log(getRandomId(10000000000000000000,0))

let simulatedFunctions = {
  'sendAlert': function () {
    let sampleSkeleton = fs.readFileSync(path.resolve(__dirname, 'sample_skeleton.json'), 'utf8')
    let now = new Date()
    // now = now.setMinutes(now.getMinutes() - 8);
    // now = now.setHours(now.getHours() - 20);
    // now = now.setDate(now.getDate() + 1);
    now = Math.floor(now / 1000) // 1538069780889
    let jsonAlert = {
      eventType: 1,
      // personId: -1,
      personId: 110,
      time: now,
      skeleton: { data: sampleSkeleton }, // must have
      fileVersion: '1.0',
      level: 'RED' // must have, for demo, it will always RED
    }
    client.publish('client/' + thisDevice.serialNumber + '/notify', JSON.stringify(jsonAlert), pubOpt)
    console.log('Publishing to:' + 'client/' + thisDevice.serialNumber + '/notify')
  },
  'fetchCameraView': function (data, message) {
    console.log('At FetchCameraView')
    let cameraViewImage = fs.readFileSync('test.jpg')
    let retObj = cameraViewImage
    client.publish('client/' + data[0] + '/view/' + data[2], retObj, pubOpt)
    console.log('Publishing to: ' + 'client/' + data[0] + '/query/' + data[2])
  },

  'fetchInfo': function (data, message) {
    console.log('At FetchInfo')
    let retObj = {
      'request': data[3],
      'code': 0,
      'data': {
        token: thisDevice.token,
        friendlyDescription: thisDevice.friendlyDescription,
        version: thisDevice.version,
        friendlyName: thisDevice.friendlyName,
        serialNumber: thisDevice.serialNumber,
        nightVision: thisDevice.nightVision,
        ssid: thisDevice.ssid,
        wifiStrength: thisDevice.wifiStrength,
        status: thisDevice.status
      }
    }
    client.publish('client/' + data[0] + '/query/' + data[2], JSON.stringify(retObj), pubOpt)
    console.log('Publishing to:' + 'client/' + data[0] + '/query/' + data[2])
  },

  'token': function (data, message) {
    console.log('At to token')
    thisDevice.token = parseInt(message.toString('utf8'))
    writeDevice(thisDevice)
    let retObj = {
      'request': data[3],
      'code': 0,
      'data': {}
    }
    client.publish('client/' + data[0] + '/query/' + data[2], JSON.stringify(retObj), pubOpt)
    console.log('Publishing to:' + 'client/' + data[0] + '/query/' + data[2])
  },

  'friendlyName': function (data, message) {
    console.log('At friendlyName')
    thisDevice.friendlyName = message.toString('utf8')
    writeDevice(thisDevice)
    let retObj = {
      'request': data[3],
      'code': 0,
      'data': {}
    }
    client.publish('client/' + data[0] + '/query/' + data[2], JSON.stringify(retObj), pubOpt)
    console.log('Publishing to:' + 'client/' + data[0] + '/query/' + data[2])
  },

  'friendlyDescription': function (data, message) {
    console.log('At friendlyDescription')
    thisDevice.friendlyDescription = message.toString('utf8')
    writeDevice(thisDevice)
    let retObj = {
      'request': data[3],
      'code': 0,
      'data': {}
    }
    client.publish('client/' + data[0] + '/query/' + data[2], JSON.stringify(retObj), pubOpt)
    console.log('Publishing to:' + 'client/' + data[0] + '/query/' + data[2])
  },

  'nightVision': function (data, message) {
    console.log('At nightVision')
    thisDevice.nightVision = message.toString('utf8')
    writeDevice(thisDevice)
    let retObj = {
      'request': data[3],
      'code': 0,
      'data': {}
    }
    client.publish('client/' + data[0] + '/query/' + data[2], JSON.stringify(retObj), pubOpt)
    console.log('Publishing to:' + 'client/' + data[0] + '/query/' + data[2])
  },

  'deleteCamera': function (data, message) {
    console.log('At DeleteCamera')
    thisDevice.token = -1
    thisDevice.friendlyDescription = 'Default Message'
    thisDevice.friendlyName = 'Altumview Camera'
    thisDevice.faces = []
    writeDevice(thisDevice)
    let retObj = {
      'request': data[3],
      'code': 0,
      'data': {}
    }
    client.publish('client/' + data[0] + '/query/' + data[2], JSON.stringify(retObj), pubOpt)
    console.log('Publishing to:' + 'client/' + data[0] + '/query/' + data[2])
  },

  'addFace': function (data, message) {
    console.log('At AddFace')
    let personId = data[4]
    let faceId = data[5]

    for (let i = 0; i < thisDevice.faces.length; i++) {
      console.log('faceId referenced from JSON: ' + thisDevice.faces[i].faceId)
      console.log('faceId of query: ' + faceId)
      if (thisDevice.faces[i].faceId === faceId) {
        console.log('adding same face')
        let retObj = {
          'request': data[3],
          // "code": 97, no need for code 97, if it is exists all good, just dun rewrite but return code 0
          'code': 0,
          'data': faceId
        }

        client.publish('client/' + data[0] + '/face/' + data[2], JSON.stringify(retObj), pubOpt)
        console.log('Publishing to:' + 'client/' + data[0] + '/face/' + data[2])
        return
      }
    }

    console.log('No same face was detected, adding face now')

    let faceImageDetails = { personId: personId, faceId: faceId, faceData: message.toString('hex') }

    thisDevice.faces.push(faceImageDetails)

    writeDevice(thisDevice)
    let retObj = {
      'request': data[3],
      'code': 0,
      'data': faceId
    }
    client.publish('client/' + data[0] + '/face/' + data[2], JSON.stringify(retObj), pubOpt)
    console.log('Publishing to:' + 'client/' + data[0] + '/face/' + data[2])
  },

  'deleteFace': function (data, message) {
    console.log('At DeleteFace')
    let faceId = data[4]

    for (let i = 0; i < thisDevice.faces.length; i++) {
      console.log('faceId referenced from JSON: ' + thisDevice.faces[i].faceId)
      console.log('faceId of query: ' + faceId)

      if (thisDevice.faces[i].faceId === faceId) {
        thisDevice.faces.splice(i, 1)
        console.log('removed face')
        writeDevice(thisDevice)

        let retObj = {
          'request': data[3],
          'code': 0,
          'data': faceId
        }

        client.publish('client/' + data[0] + '/face/' + data[2], JSON.stringify(retObj), pubOpt)
        console.log('Publishing to:' + 'client/' + data[0] + '/face/' + data[2])
        return
      }
    }

    console.log('FaceId ' + faceId + ' could not be found')

    let retObj = {
      'request': data[3],
      'code': 0,
      'data': faceId
    }
    client.publish('client/' + data[0] + '/face/' + data[2], JSON.stringify(retObj), pubOpt)
    console.log('Publishing to:' + 'client/' + data[0] + '/face/' + data[2])
  },

  'firmware': function (data, message) {
    console.log('At firmware')
    console.log('Got message:', message)
    thisDevice.version = data[4].toString('utf8')
    writeDevice(thisDevice)
    let retObj = {
      'request': data[3],
      'code': 0,
      'data': {}
    }
    client.publish('client/' + data[0] + '/query/' + data[2], JSON.stringify(retObj), pubOpt)
    console.log('Publishing to:' + 'client/' + data[0] + '/query/' + data[2])
  },

  'replyUndefined': function (data, message) {
    console.log('At replyUndefined')
    let retObj = {
      'request': data[3],
      'code': 99,
      'data': data
    }
    client.publish('client/' + data[0] + '/query/' + data[2], JSON.stringify(retObj), pubOpt)
    console.log('Publishing to:' + 'client/' + data[0] + '/query/' + data[2])
  }
}
console.log(args)

// var simulateAlert = simulatedFunctions["sendAlert"];
// for (var i = 2; i >= 0; i--) {
//     simulateAlert();
// }
// SIMULATED CLIENT PART

client.on('connect', function (connack) {
  console.log('connected')

  if (connack.sessionPresent !== true) {
    console.log('session exists')
    client.subscribe(`sdmc/${thisDevice.serialNumber}/#`, { qos: 2 })
  }

  let faceArray = []

  thisDevice.faces.forEach(function (face) {
    faceArray.push(parseInt(face.faceId))
  })

  let connect = {
    request: 'connectCamera',
    token: thisDevice.token,
    faces: faceArray
  }

  let connectMessageOpt = { qos: 1, retain: true }
  client.publish('client/' + thisDevice.serialNumber + '/connect', JSON.stringify(connect), connectMessageOpt)
  console.log('Connected: sdmc/' + thisDevice.serialNumber + '/#')
})

client.on('close', function () {
  console.log('disconnected')
})

client.on('reconnect', function () {
  console.log('starting to reconnect')
})

client.on('error', function (error) {
  console.log('Client cannot connect or parsing error occured: ', error)
})

client.on('message', function (topic, message) {
  console.log('MQTT CLIENT GOT MESSAGE FROM TOPIC: ' + topic)
  let data = topicSubdata(topic, 'sdmc')
  let operationType = data[3]
  console.log('topic as arrays: ' + data)
  console.log('operation in topc: ' + operationType)
  console.log('operationType typeof: ' + typeof operationType)
  console.log('message: ' + message)

  let operationToDo = simulatedFunctions[operationType] === undefined ? simulatedFunctions['replyUndefined'] : simulatedFunctions[operationType]

  operationToDo(data, message)

  let stateValue = 'State: '
  if (thisDevice.recording) stateValue += '(recording)'
  if (thisDevice.on) stateValue += '(on)'
  if (!thisDevice.on) stateValue += '(off)'
  stateValue += '(nightVision = ' + thisDevice.nightVision + ')'
  thisDevice.faces.forEach(function (faceDetailobj) {
    stateValue += '(FaceId ' + faceDetailobj.faceId + ')'
  })
  console.log(stateValue)
})
