'use strict'
// PROBLEM:
//
const mqtt = require('mqtt')
var sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
function binaryInsert (value, array, startVal, endVal) {
  var length = array.length
  var start = typeof (startVal) !== 'undefined' ? startVal : 0
  var end = typeof (endVal) !== 'undefined' ? endVal : length - 1
  var m = start + Math.floor((end - start) / 2)
  if (length === 0) {
    array.push(value)
    return
  }
  if (value > array[end]) {
    array.splice(end + 1, 0, value)
    return
  }
  if (value < array[start]) {
    array.splice(start, 0, value)
    return
  }
  if (start >= end) {
    return
  }
  if (value < array[m]) {
    binaryInsert(value, array, start, m - 1)
    return
  }
  if (value > array[m]) {
    binaryInsert(value, array, m + 1, end)
  }
}

class SimpleElevator {
  constructor (id, curFloor) {
    this.id = id
    this.operating = 0
    this.direction = 0
    this.task = []
    this.doorOpened = 0
    this.sendStatus = 0
    this.curFloor = curFloor
  }

  async request (from, to) {
    // if (this.direction === 'stationary') {
    //   if (this.curFloor === from) {
    //     await this.OpenElevatorDoor()
    //     this.direction = this.curFloor < to ? 'up' : 'down'
    //     this.task.push(to)
    //   } else if (this.curFloor > from) {
    //     this.direction = 'down'
    //     this.task.push(from)
    //     this.task.push(to)
    //   } else if (this.curFloor < from) {
    //     this.direction = 'up'
    //     this.task.push(from)
    //     this.task.push(to)
    //   }
    // } else {
    //   this.task.push(from)
    //   this.task.push(to)
    // }
    this.task.push(from)
    this.task.push(to)
  }

  async OpenElevatorDoor (timeout) {
    let _timeout = typeof (timeout) !== 'undefined' ? timeout * 1000 : 5000
    if (this.doorOpened === 0) {
      this.doorOpened = 1
      this.DoSendStatus()
      console.log(`Door open`)
      await sleep(_timeout)
      console.log(`Door closed`)
      this.doorOpened = 2
      this.DoSendStatus()
    }
  }

  Stop () {
    this.operating = 0
  }

  DoSendStatus () {
    let sendObject = {
      'command': 97,
      'liftId': this.id,
      'state': {
        'Floor': this.curFloor,
        'Direction': this.direction,
        'Door state': this.doorOpened,
        'Registered Floors': this.task,
        'Load': 1
      }
    }
    client.publish(elevatorPubTopic, JSON.stringify(sendObject), pubOpt)
  }

  async Start () {
    this.operating = 1
    while (this.operating) {
      console.log(`Elevator status: [${this.direction}] [${this.curFloor}]`)
      console.log(`task list: ${this.task}`)
      if (this.task.length !== 0) {
        let _nextFloor = this.task[0]
        if (this.curFloor === _nextFloor) {
          await this.OpenElevatorDoor()
          this.task.splice(0, 1)
        } else if (this.curFloor > _nextFloor) {
          this.direction = 2
          await sleep(2000)
          this.curFloor -= 1
        } else if (this.curFloor < _nextFloor) {
          this.direction = 1
          await sleep(2000)
          this.curFloor += 1
        }
        this.DoSendStatus()
      } else {
        await sleep(3000)
      }
    }
  }
}

class Elevator {
  constructor (id, curFloor) {
    this.id = id
    this.operating = 0
    this.direction = 'stationary'
    this.up_task = []
    this.down_task = []
    this.curFloor = curFloor
    this.doorOpened = 0
  }
  async request (from, to) {
    if (this.direction === 'stationary') {
      if (this.curFloor === from) {
        await this.OpenElevatorDoor()
        this.direction = this.curFloor < to ? 'up' : 'down'
        if (this.direction === 'up') {
          binaryInsert(to, this.up_task)
        } else {
          binaryInsert(to, this.down_task)
        }
      } else if (this.curFloor > from) {
        this.direction = 'down'
        binaryInsert(from, this.down_task)
        if (from > to) {
          binaryInsert(to, this.down_task)
        } else {
          binaryInsert(to, this.up_task)
        }
      } else if (this.curFloor < from) {
        this.direction = 'up'
        binaryInsert(from, this.up_task)
        if (from > to) {
          binaryInsert(to, this.down_task)
        } else {
          binaryInsert(to, this.up_task)
        }
      }
    } else {
      if (this.curFloor > from) {
        binaryInsert(from, this.down_task)
        if (from > to) {
          binaryInsert(to, this.down_task)
        } else {
          binaryInsert(to, this.up_task)
        }
      } else if (this.curFloor < from) {
        binaryInsert(from, this.up_task)
        if (from > to) {
          binaryInsert(to, this.down_task)
        } else {
          binaryInsert(to, this.up_task)
        }
      } else {
        if (this.direction === 'up') {
          if (this.curFloor < to) {
            await this.OpenElevatorDoor()
            binaryInsert(to, this.up_task)
          } else {
            binaryInsert(from, this.down_task)
            binaryInsert(to, this.down_task)
          }
        } else {
          if (this.curFloor > to) {
            await this.OpenElevatorDoor()
            binaryInsert(to, this.down_task)
          } else {
            binaryInsert(from, this.up_task)
            binaryInsert(to, this.up_task)
          }
        }
      }
    }
  }
  async OpenElevatorDoor (timeout) {
    let _timeout = typeof (timeout) !== 'undefined' ? timeout * 1000 : 5000
    if (this.doorOpened === 0) {
      this.doorOpened = 1
      console.log(`Door open`)
      await sleep(_timeout)
      console.log(`Door closed`)
      this.doorOpened = 0
    }
  }
  Stop () {
    this.operating = 0
  }
  async Start () {
    this.operating = 1
    while (this.operating) {
      console.log(`Elevator status: [${this.direction}] [${this.curFloor}] 
                   [Door open: ${this.doorOpened}]`)
      console.log(`up_queue: ${this.up_task}`)
      console.log(`down_queue: ${this.down_task}`)
      if (this.direction === 'up') {
        if (this.up_task.length !== 0) {
          let _nextFloor = this.up_task[0]
          if (this.curFloor === _nextFloor) {
            await this.OpenElevatorDoor()
            this.up_task.splice(0, 1)
          } else {
            if (this.doorOpened === 0) {
              await sleep(2000)
              this.curFloor += 1
            }
          }
        } else {
          if (this.down_task.length !== 0) {
            this.direction = 'down'
            let _nextFloor = this.down_task[this.down_task.length - 1]
            if (this.curFloor === _nextFloor) {
              await this.OpenElevatorDoor()
              this.down_task.splice(this.down_task.indexOf(_nextFloor), 1)
            } else {
              if (this.doorOpened === 0) {
                await sleep(2000)
                this.curFloor -= 1
              }
            }
          } else {
            this.direction = 'stationary'
          }
        }
      } else if (this.direction === 'down') {
        if (this.down_task.length !== 0) {
          let _nextFloor = this.down_task[this.down_task.length - 1]
          if (this.curFloor === _nextFloor) {
            await this.OpenElevatorDoor()
            this.down_task.splice(this.down_task.indexOf(_nextFloor), 1)
          } else {
            if (this.doorOpened === 0) {
              await sleep(2000)
              this.curFloor -= 1
            }
          }
        } else {
          if (this.up_task.length !== 0) {
            this.direction = 'up'
            let _nextFloor = this.up_task[0]
            if (this.curFloor === _nextFloor) {
              await this.OpenElevatorDoor()
              this.up_task.splice(0, 1)
            } else {
              if (this.doorOpened === 0) {
                await sleep(2000)
                this.curFloor += 1
              }
            }
          } else {
            this.direction = 'stationary'
          }
        }
      } else {
        await sleep(2000)
      }
    }
  }
}

const pubOpt = { qos: 2 }
const ELEVATOR_GROUP_ID = 'elevator_123'
const elevatorPubTopic = `lift/${ELEVATOR_GROUP_ID}/notify`
const elevator_1 = new Elevator('elevator_1', 1)
const options =
  {
    reconnectPeriod: 1000,
    resubscribe: true,
    clientId: ELEVATOR_GROUP_ID,
    clean: false
  }

const client = mqtt.connect('mqtt://elevate.altumview.com:1883', options)

// const client = mqtt.connect('mqtt://localhost:1883', options)

function parseMessage (message) {
  try {
    let json = JSON.parse(message)
    return json
  } catch (err) {
    console.log(err)
    sendReplyMessage({ type: 'parseError', command: '101', data: message.toString() })
  }
}

function sendReplyMessage (options) {
  let sendObject = {}
  switch (options.type) {
    case 'default':
      sendObject = {
        'command': options.command,
        'taskId': options.taskId,
        'seqId': options.seqId,
        'errcode': options.errCode
      }
      client.publish(elevatorPubTopic, JSON.stringify(sendObject), pubOpt)
      break
    case 'withLiftId':
      sendObject = {
        'command': options.command,
        'taskId': options.taskId,
        'seqId': options.seqId,
        'errcode': options.errCode,
        'liftId': options.data
      }
      client.publish(elevatorPubTopic, JSON.stringify(sendObject), pubOpt)
      break
    case 'parseError':
      sendObject = {
        'command': 'error in parsing message body',
        'message': options.data
      }
      client.publish(elevatorPubTopic, JSON.stringify(sendObject), pubOpt)
      break
    case 'invalidCommand':
      sendObject = {
        'command': options.command,
        'errCode': 'invalid command'
      }
      client.publish(elevatorPubTopic, JSON.stringify(sendObject), pubOpt)
      break
  }
}

function designateTask (location, destination) {
  return Math.floor(Math.random() * Math.floor(5))
}

const simulatedFunctions = {
  1: function (parsed) {
    console.log('registering Elevator')
    let elevatorID = elevator_1.id
    let _errCode = 0
    let options = {
      type: 'withLiftId',
      command: parsed['command'],
      taskId: parsed['taskId'],
      seqId: parsed['seqId'],
      errCode: _errCode,
      data: elevatorID
    }
    sendReplyMessage(options)
    elevator_1.request(parsed['from'], parsed['to'])
  },
  2: function (parsed) {
    console.log('registering target floor')
    let _errCode = 0
    let options = {
      type: 'default',
      command: parsed['command'],
      taskId: parsed['taskId'],
      seqId: parsed['seqId'],
      errCode: _errCode
    }
    sendReplyMessage(options)
  },
  3: function (parsed) {
    console.log('Open the door')
    let _errCode = 0
    let options = {
      type: 'default',
      command: parsed['command'],
      taskId: parsed['taskId'],
      seqId: parsed['seqId'],
      errCode: _errCode
    }
    sendReplyMessage(options)
  },
  'replyUndefined': function (parsed) {
    console.log('reply undefined')
    let options = {
      type: 'invalidCommand',
      command: parsed.command,
      errCode: 'invalid command type'
    }
    sendReplyMessage(options)
  }
}

client.on('connect', function (connack) {
  console.log('connected')

  if (connack.sessionPresent !== true) {
    console.log('session does not exist')
    client.subscribe(`lift/${ELEVATOR_GROUP_ID}/command`, { qos: 2 })
  }
  elevator_1.Start()
})

client.on('close', function () {
  console.log('A disconnection occured')
})

client.on('offline', function () {
  console.log('client has gone offline')
})

client.on('reconnect', function () {
  console.log('starting to reconnect')
})

client.on('error', function (error) {
  console.log('Client cannot connect or parsing error occured: ', error)
})

client.on('message', function (topic, message) {
  console.log(`[${topic}] got message <${message}>`)
  let parsed = parseMessage(message)
  if (parsed) {
    console.log(`parsed message: ${JSON.stringify(parsed)}`)
    let operationType = parsed['command']
    let operationToDo = simulatedFunctions[operationType] === undefined ? simulatedFunctions['replyUndefined'] : simulatedFunctions[operationType]
    operationToDo(parsed)
  }
})
