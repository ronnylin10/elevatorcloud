const mqtt = require('mqtt')
const readline = require('readline')
const ROBOT_ID = 'Robot123'
const ELEVATOR_GROUP_ID = 'elevator_123'
const robotPubTopic = 'lift/' + ELEVATOR_GROUP_ID + '/command'
const pubOpt = { qos: 2 }
const options =
  {
    reconnectPeriod: 1000,
    resubscribe: true,
    clientId: ROBOT_ID,
    clean: false
  }

const cin = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

function registerElevator (from, to) {
  let sendObject = {
    'command': 1,
    'taskId': 'task ID',
    'seqId': 'command serial number',
    'from': from,
    'to': to
  }
  client.publish(robotPubTopic, JSON.stringify(sendObject), pubOpt)
}

function registerTargetFloor () {
  let sendObject = {
    'command': 2,
    'taskId': 'task ID',
    'seqId': 'command serial number',
    'liftId': 'serial number in the group',
    'to': 'target floor'
  }
  client.publish(robotPubTopic, JSON.stringify(sendObject), pubOpt)
}

function OpenDoor () {
  let sendObject = {
    'command': 3,
    'taskId': 'task ID',
    'seqId': 'command serial number',
    'liftId': 'serial number in the group',
    'time': 'holding period'
  }
  client.publish(robotPubTopic, JSON.stringify(sendObject), pubOpt)
}

function invalidCommand () {
  let sendObject = {
    'command': 20
  }
  client.publish(robotPubTopic, JSON.stringify(sendObject), pubOpt)
}

function badMessage () {
  client.publish(robotPubTopic, 'None JSON', pubOpt)
}
function promptInput () {
  cin.question('1 for register elevator, 2 for registerTargetFloor, 3 for OpenDoor, 0 for exit: ', (answer) => {
    switch (answer) {
      case '1':
        registerElevator(1, 5)
        console.log('registerElevator')
        return promptInput()

      case '2':
        registerTargetFloor()
        console.log('registerTarget')
        return promptInput()

      case '3':
        OpenDoor()
        console.log('OpenDoor')
        return promptInput()
      case '0':
        client.publish(robotPubTopic, 'exit', pubOpt)
        cin.close()
        client.end()
        console.log('closed')
        return 0
      default:
        console.log('undefined response for: ', answer)
        return promptInput()
    }
  })
}

const client = mqtt.connect('mqtt://localhost:1883', options)
client.on('connect', function (connack) {
  console.log('connected')
  promptInput()
  if (connack.sessionPresent !== true) {
    console.log('session does not exist')
    client.subscribe(`lift/${ELEVATOR_GROUP_ID}/notify`, { qos: 2 })
  }
})

client.on('message', function (topic, message) {
  console.log(`[${topic}]`)
  try {
    let json = JSON.parse(message.toString())
    console.log(JSON.stringify(json))
  } catch (err) {
    console.log(err)
  }
})
