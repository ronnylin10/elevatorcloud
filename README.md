# MQTT Broker

This document will show you what you need in order to run the new MQTT broker along side the cloud services.

### Requirements
- docker - version 18 and up.
- nodejs - version 10.8.0 and up. 
- npm - version 6.2.0 and up.

### Steps
1. Navigate to the broker directory and install the dependencies.
```sh
$ cd path/to/mqtt_broker
$ npm install
```
2. Start MongoDB container.
```sh
$ sudo docker run --name mqtt_mongodb -d -p 27017:27017 -v /path/to/mqtt_broker/data:/data/db mongo
```
if the container is already running, then you do not need to go through this step.
3. Start the broker. 
```sh
$ sudo node mqtt_server.js &
```
The console will print ``` server listening on port 1883 ``` after the last command which means the broker is now listening on that port.

4. To stop the broker, just kill the process.
```
sudo kill ${process_id}
```

### Notes
- Modify the config.json file to change configuration for the MQTT broker. 
- DO NOT stop the container running the database. Doing so will cause issues. Instead, use ```docker container pause```.
- Copy the MQTT broker folder from the repository to another directory in the local machine first before running it.  

